package com.beiyoufx.demo.dao;

import com.beiyoufx.demo.entity.Product;

/**
 * @author yongjie.teng
 * @date 17-2-4 下午4:25
 * @email yongjie.teng@foxmail.com
 * @package com.beiyoufx.demo.dao
 */
public class ProductDao extends BaseDao<Product> {
}
