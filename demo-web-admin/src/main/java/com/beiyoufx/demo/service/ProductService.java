package com.beiyoufx.demo.service;

import com.beiyoufx.demo.entity.Product;

/**
 * @author yongjie.teng
 * @date 17-2-4 下午4:26
 * @email yongjie.teng@foxmail.com
 * @package com.beiyoufx.demo.service
 */
public class ProductService extends BaseService<Product> {
}
